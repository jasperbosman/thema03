Finally, time to add some dynamic content to our site!

This week you will create a HTML page with a form that allows an user to upload visualization settings to the server. 
The user should get instructions on what he/she can fill in the form fields.
