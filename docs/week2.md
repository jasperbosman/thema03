This week you will learn how to create a simple static HTML page and style it using CSS. See the *HTML_template.html* and *CSS_template.css* under the `lib/templates/HTML_CSS_templates` folder to get you going.

Create a valid HTML 5 pages that will have a page containing information regarding the project and a page describing the chosen format.
Add a menu that will allow users to navigate the different pages. Also include a footer with contact information. Use CSS3 styling in an external stylesheet document to add styling to your pages.

HTML and CSS pages should be validated. The following online tools can be used to validate:

* [HTML5](https://html5.validator.nu/); select as validator input: *text field*
* [CSS3](https://jigsaw.w3.org/css-validator/#validate_by_input)