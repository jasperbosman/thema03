# Thema03
This repo contains the example code and exercises needed for Theme03 of the Bioinformatics practical course of the Hanze University of Applied Sciences.
The repo can be used to learn how to setup a basic Flask web application. It contains example code and exercises on different concepts from the Flask framework. 
HTML and CSS are both part of the web application developed during this theme, simple template files are available which can be used as a starting point.
To get this code running follow the [Installation guidelines for this project](docs/installation.md).


## Table of content
1. [Installation](docs/installation.md)
2. [Weekly Assignments](#markdown-header-assignments)
    1. [Week1](docs/week1.md)
    2. [Week2](docs/week2.md)
    3. [Week3](docs/week3.md)
    4. [Week4](docs/week4.md)
    5. [Week5](docs/week5.md)
    6. [Week6](docs/week6.md)
    7. [Week7](docs/week7.md)
3. [Grading](#markdown-header-grading)
4. [Exercises](#markdown-header-exercises)


## Assignments
All assignments should be handed in by uploading them to Blackboard.
Please check the following requirements before you upload any assignment. Failing to adhere to these requirements will lead to the assignment not being scored.

* Pack the assignment and any relevant files into a single **zip** file named: yourName_Assignment<number>.zip. When unpacked there should be a single folder named: yourName_Assignment<number> 
* Always include a readme.txt file
* All other text documents (wordfiles, powerpoints, etc) as PDF
* Your code needs to be commented (docstring, blockcomments and inline)
* Code is compliant with the PEP8 styleguide
* Valid HTML and CSS files
* HTML and CSS also needs to be commented
* Supply test code and files!


## Grading
The theme will be graded based on 3 products combined with the evaluation of the professional work attitude.

*	10% project proposal (1 A4), including visualisation examples + Presentation
*	70% end product (webapplication)
*	10% poster + flash
*	10% professional work attitude 


## Exercises
During lectures the *run_app.py* will be used to demonstrate the Flask concepts. The demos also contain exercises for you to try and implement. These can be found under the lib folder. 
To start the Flask server simply remove the comment of the Flask app you want to start and run the *run_app.py* from Pycharm. The following demos/exercises are available.

|Exercise|Topic|
|-------|-----|
|basic_Flask_app       |Minimal Flask app returning Hello Wold!|
|Flask_app_without_templating       |Printing HTML from within the Flask app|
|Flask_app_with_templating| Flask app with Jinja2 templating|
|simple_routing| Routing explained|
|creating_your_own_error_page| Error Handling|
|redirections| Flask redirection|
|form_handling| Form handling|
|Flask_static_folder| Static files|
|Jinja2_inheritance| Jinja2 inheritance|
