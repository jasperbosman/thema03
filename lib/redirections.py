from flask import Flask, redirect, url_for, render_template

app = Flask(__name__)


@app.route('/')
def root():
    return redirect(url_for('all_your_base'))


# 301 means ULR moved permanently, 302 is default for redirect and is for temporary relocating
@app.route('/index')
def index():
    return redirect(url_for('all_your_base'), code=301)


@app.route('/courses')
def courses():
    return redirect(url_for('all_your_base'), code=302)


@app.route('/all_your_base')
def all_your_base():
    return render_template('simple_routing/redirection.html')