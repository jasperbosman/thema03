from flask import Flask, request, render_template

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def simple_form_handling():
    if request.method == 'GET':
        return render_template('form/form_GET.html')
    elif request.method == 'POST':
        kwargs = {
            'course': request.form['course'],
            'ec': request.form['ec'],
            'teacher': request.form['teacher'],
        }
        return render_template('form/form_POST.html', **kwargs)
