from flask import Flask

app = Flask(__name__)


@app.route('/')
def our_first_html_page():
    page = """
    <html>
        <head>
            <title>Our first page!</title>
        </head>
        <body>
            <h1>Finally, my own website</h1>
            
            <ul>
                <li>Theme01</li>
                <li>Thema02</li>
                <li>Thema03</li>
            </ul>
        </body>
    </html>
    """

    return page