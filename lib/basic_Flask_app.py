from flask import Flask

app = Flask(__name__)

# this will bind the root of the server to the hello_world view function
@app.route('/')
def hello_world():
    return 'Hello World!'


# TODO
# add a new view function that will be bound to the address /name and will print your name