from flask import Flask, render_template_string, render_template

app = Flask(__name__)


@app.route('/')
def our_first_html_page():
    courses = ['Theme1','Theme2','Theme3','Theme4','Theme5']

    page = """
    <html>
        <head>
            <title>{{ title }}</title>
        </head>
        <body>
            <h1>Finally, my own website</h1>

            <ul>
                {% for course in courses %}
                <li>{{ course }}</li>
                {% endfor %}
            </ul>
        </body>
    </html>
    """

    rendered_page = render_template_string(page, title='My, first page', courses=courses)
    return rendered_page


# in the next view function the html code is moved to it's own location index.html under the templates folder
@app.route('/second')
def our_second_html_page():
    courses = ['Theme1','Theme2','Theme3','Theme4','Theme5']

    return render_template('index.html', title='My, second page', courses=courses)
