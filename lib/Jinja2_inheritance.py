from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def root():
    return render_template('Jinja2_inheritance/base.html')


@app.route('/add_course')
def add_course():
    if request.method == 'GET':
        return render_template('Jinja2_inheritance/form_base_inheritance.html')
